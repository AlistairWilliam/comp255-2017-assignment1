package org.bitbucket.sudoku;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;


/**
 * Test suite for checking validity of some Sudoku grids
 */
@RunWith( Parameterized.class )
public class ValidSudokuParameterisedTests {

  /**
   *  The collection of tests as an Array of (grid x expected result)
   */
  /* *INDENT-OFF* */
  @Parameters
  public static Collection<Object[]> data() {
    // testcases wil de displayed as test[0], test[1] and so on
    return Arrays.asList(new Object[][] {
      { new int[][] { }                         , true    },
      { new int[][] { { } }                     , false   },
      { new int[][] { { 1 } }                   , true    },
      { new int[][] { { 1 , 2 }, { 2 , 1 } }    , false    },
      { new int[][] { { 1 , 2 }, { 1 , 1 } }    , false    },
      { new int[][] { { 1 }, { 1 , 2 } }        , false   }
    });
  }
  /* *INDENT-ON* */

  // Test parameters
  // @link{https://github.com/junit-team/junit4/wiki/Parameterized-tests}
  private int[][] grid;
  private boolean validStatus;

  /**
   *  Constructor for the tests
   */
  public ValidSudokuParameterisedTests(int[][] input, boolean expectedValidStatus) {
    grid = input;
    validStatus = expectedValidStatus;
  }

  /**
   * Run all the tests
   */
  @Test
  public void test()  {
    assertEquals(validStatus, new Sudoku(grid).isValid());
  }
}
